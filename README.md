# Travelling Salesman Problem

The travelling salesman problem (TSP) asks the following question: "Given a list of cities and the distances between each pair of cities, what is the shortest possible route that visits each city and returns to the origin city?" It is an NP-hard problem in combinatorial optimization, important in operations research and theoretical computer science.


## Setup

Download plugins Pkg: JuMP, GLPK, Distances


## Usage

Program use input file with cities coordinates and output the most optimise distance and rout to travel between them.

## Technologies
Juliabox, Pkg pack, jump,glpk, distances plugs.

## License
[MIT](https://choosealicense.com/licenses/mit/)